package ikomlev.tools.json;

import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

public class ClasspathJsonSchemaProvider implements JsonSchemaProvider {

    private final JsonSchemaFactory jsonSchemaFactory;

    public ClasspathJsonSchemaProvider(JsonSchemaFactory jsonSchemaFactory) {
        this.jsonSchemaFactory = jsonSchemaFactory;
    }

    @Override
    public JsonSchema getSchema(String schemaResourceName) throws Exception {
        return jsonSchemaFactory.getJsonSchema("resource:" + schemaResourceName);
    }
}

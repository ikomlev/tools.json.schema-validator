package ikomlev.tools.json;

import com.fasterxml.jackson.databind.JsonNode;

public interface JacksonJsonSchemaValidator<T> {

    T validate(String schemaName, JsonNode jsonNode) throws Exception;

}

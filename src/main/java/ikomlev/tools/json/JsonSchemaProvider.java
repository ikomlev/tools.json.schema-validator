package ikomlev.tools.json;

import com.github.fge.jsonschema.main.JsonSchema;

public interface JsonSchemaProvider {

    JsonSchema getSchema(String schemaName) throws Exception;
}

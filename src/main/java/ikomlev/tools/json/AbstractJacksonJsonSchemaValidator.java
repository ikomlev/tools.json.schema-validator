package ikomlev.tools.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.report.LogLevel;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractJacksonJsonSchemaValidator<T> implements JacksonJsonSchemaValidator<T> {

    private final JsonSchemaProvider schemaProvider;

    protected AbstractJacksonJsonSchemaValidator(JsonSchemaProvider schemaProvider) {
        this.schemaProvider = schemaProvider;
    }

    @Override
    public T validate(String schemaName, JsonNode jsonNode) throws Exception {
        JsonSchema schema = schemaProvider.getSchema(schemaName);
        ProcessingReport processingReport = schema.validate(jsonNode);
        if (processingReport.isSuccess()) {
            return validCaseResult();
        } else {
            return invalidCaseResult(processingReport);
        }
    }

    protected abstract T validCaseResult();

    protected abstract T invalidCaseResult(ProcessingReport processingReport);

    protected List<String> makeErrorList(ProcessingReport processingReport) {
        List<String> errors = new ArrayList<>();
        for (ProcessingMessage processingMessage : processingReport) {
            if (LogLevel.ERROR.equals(processingMessage.getLogLevel())) {
                JsonNode processingMessageJson = processingMessage.asJson();
                String pointer = processingMessageJson.get("instance").get("pointer").textValue();
                String message = processingMessageJson.get("message").textValue();
                errors.add(pointer.isEmpty() ? message : pointer + ": " + message);
            }
        }
        return errors;
    }

}

package ikomlev.tools.json;

import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import java.util.Collections;
import java.util.List;

public class DefaultJacksonJsonSchemaValidator extends AbstractJacksonJsonSchemaValidator<List<String>> {

    public DefaultJacksonJsonSchemaValidator() {
        super(new ClasspathJsonSchemaProvider(JsonSchemaFactory.byDefault()));
    }

    public DefaultJacksonJsonSchemaValidator(JsonSchemaProvider jsonSchemaProvider) {
        super(jsonSchemaProvider);
    }

    @Override
    protected List<String> validCaseResult() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> invalidCaseResult(ProcessingReport processingReport) {
        return makeErrorList(processingReport);
    }

}

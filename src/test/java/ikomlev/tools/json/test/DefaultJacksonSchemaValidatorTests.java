package ikomlev.tools.json.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import ikomlev.tools.json.DefaultJacksonJsonSchemaValidator;
import ikomlev.tools.json.JacksonJsonSchemaValidator;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DefaultJacksonSchemaValidatorTests {

    private static final String SIMPLE_SCHEMA = "/schemas/simple.json";

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final JacksonJsonSchemaValidator<List<String>> validator = new DefaultJacksonJsonSchemaValidator();

    @Test
    void validCase() throws Exception {
        InputStream resourceStream = getClass().getResourceAsStream("/simpleValid.json");
        List<String> errorList = validator.validate(SIMPLE_SCHEMA, objectMapper.readTree(resourceStream));
        assertTrue(errorList.isEmpty());
    }

    @Test
    void invalidCase() throws Exception {
        InputStream resourceStream = getClass().getResourceAsStream("/simpleInvalid.json");
        List<String> errorList = validator.validate(SIMPLE_SCHEMA, objectMapper.readTree(resourceStream));
        assertFalse(errorList.isEmpty());
    }

}
